# Parallel Programming Exercises

Various exercices on parallel programming, produced during the Parallel Calculation course at my university. The exercises include some Bash and Python script that i created to automate the testing phase on the university's parallel cluster.

### Dependency

* MPI
* OpenMPI
* CUDA 7.5

### Notes

'A[O]scar(ius)', 'aoscarius' and 'ascarius' are all my nickname.

### Licenses

All this material are released under GPL License
Copyright (C) 2015 Oscar Castello

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
