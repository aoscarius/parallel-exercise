#include <stdio.h>
#ifdef _OPENMP
	#include <omp.h>
#endif

int main(int argc, char* argv[])
{
	#ifdef _OPENMP
		#pragma omp parallel
		{
			printf("Hello dal thread %d\n", omp_get_thread_num());
		}
	#else
		printf("Questa macchina non supporta la libreria OpenMP\n");
	#endif
	return 0;
}
