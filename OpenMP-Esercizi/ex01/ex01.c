/*******************************************
* Code autogenerated with ibrid_genproject tool
* Author: ascarius
* Date: Thu Nov 11 10:49:44 CEST 2015
********************************************/

#include <stdio.h>
#include <omp.h>

int main( int argc, char *argv[] )
{
	// Setting of thread number
	omp_set_num_threads(2);
	printf("Esecuzione con %d thread:\n", 2);
	#pragma omp parallel
	{
		printf("Io sono il thread %d di %d\n",omp_get_thread_num(), omp_get_num_threads());
	}
	printf("\n");

	// Setting of thread number
	omp_set_num_threads(4);
	printf("Esecuzione con %d thread:\n", 4);
	#pragma omp parallel
	{
		printf("Io sono il thread %d di %d\n",omp_get_thread_num(), omp_get_num_threads());
	}
	printf("\n");

	// Setting of thread number
	omp_set_num_threads(6);
	printf("Esecuzione con %d thread:\n", 6);
	#pragma omp parallel
	{
		printf("Io sono il thread %d di %d\n",omp_get_thread_num(), omp_get_num_threads());
	}
	printf("\n");

    return 0;
}
