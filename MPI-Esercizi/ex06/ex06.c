/*******************************************
* Code autogenerated with ./genproject tool
* Author: Oscar Castello
* Date: Mon Oct 12 08:49:49 CEST 2015
********************************************/

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

#define DIM 40
#define SUBSIZE 10

int main( int argc, char *argv[] )
{
    int myrank, nprocs;

	int i, *svect, *rvect;

    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&nprocs);
    MPI_Comm_rank(MPI_COMM_WORLD,&myrank);

    if (DIM%nprocs != 0) {
       printf("Questo algoritmo richiede esattamente %d proc.\n", DIM/SUBSIZE); 
       MPI_Abort(MPI_COMM_WORLD, 1);
    } else {
       	if (myrank==0) {
		// Solo se sono il primo processo inizializzo il vettore
			svect = (int *)malloc(DIM * sizeof(int));
			printf("Io sono il processo %d e ho generato l'array:\n",myrank);
			for(i=0; i<DIM; i++) printf("%d ",svect[i]=i);
			printf("\n");
		}
		rvect= (int *)malloc(SUBSIZE * sizeof(int));
		MPI_Scatter(svect, SUBSIZE, MPI_INT, rvect, SUBSIZE, MPI_INT, 0,MPI_COMM_WORLD);
		printf("\nIo sono il processo %d e ho ricevuto l'array \n", myrank);
		for(i=0; i<SUBSIZE; i++) printf("%d ",rvect[i]);
		fflush(stdout);
		printf("\n");

    }

    MPI_Finalize();
    return 0;
}
