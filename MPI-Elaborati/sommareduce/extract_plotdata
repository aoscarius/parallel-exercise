#!/bin/bash

# MPI Extract-Plotting Utility Script GPL Source Code
# Copyright (C) 2015 Oscar Castello
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# ============================================================
#                     Variable Settings
# ============================================================
CURFNAME=sommareduce

DSIZE=(1000 5000 10000 100000 500000)

CPU=(1 2 4 6 8 16 32)

pltCPU=8

TDATA=$(
for (( i=0; i<${#DSIZE[@]}; i++ ))
do
	cat t_$CURFNAME.out | perl -e '
		print "T.append(["; 
		while (<>) { 
			if ( $_ =~ /^L.algoritmo ha sommato '${DSIZE[i]}' numeri su (.+) proc. in (.+) us$/ ) { 
				print "$2, ";	
			} 
		} print "])\n";
	'
done
)
# ============================================================


# ============================================================
#				     Executable Script
# ============================================================
function join { local IFS="$1"; shift; echo "$*"; }

if ! [[ -e data ]]; then mkdir data; fi
if [[ -e ./data/plotdata.py ]]; then rm ./data/plotdata.py; fi

cat << _EOF_ > ./data/plotdata.py
#!/usr/bin/env python
from __future__ import division
import matplotlib.pyplot as plt

# ==========  Parametri definiti dall'esterno  ==============
CPUMAX=$pltCPU
CPU=[$(join , ${CPU[@]})]
DSIZE=[$(join , ${DSIZE[@]})]

T=[]
${TDATA[@]}

# Definizione funzione di ricerca massimo all'interno di una matrice
def max(V):
	vmax = 0
	for vrow in V:
		for i in range(0,vrow.__len__()):
			if ((CPU[i]<=CPUMAX) and (vrow[i]>vmax)):
				vmax=vrow[i]
	return vmax

def writeTexTable(fname, table):
	out_file = open(fname,"w")
	out_file.write("\\\\begin{tabular}{|%s|}\n\\\\hline\n"%("|".join(("c") for i in range(len(CPU)+1))))
	out_file.write("\\\\textbf{N\\\\textbackslash{}CPU} & %s \\\\tabularnewline\n\\\\hline\n\\\\hline\n"%(" & ".join(map(r'\textbf{{{0}}}'.format, CPU))))
	for i in range(len(DSIZE)):
		out_file.write("\\\\textbf{%s} & %s \\\\tabularnewline\n\\\\hline\n"%(DSIZE[i]," & ".join(map('{0}'.format, table[i]))))
	out_file.write("\\\\end{tabular}\n")
	out_file.close()

# Definizione colori base per il plotting
C=['c','b','g','r','m','y','k']
CLR=['cyan','blue','green','red','magenta','yellow','black']

# Calcolo speed-up
S=[]
for Tline in T:
	Sline=[]
	for i in range(0,Tline.__len__()):
		Sline.append(round(Tline[0]/Tline[i],4))
	S.append(Sline)

# Calcolo overhead
O=[]
for Tline in T:
	Oline=[]
	for i in range(0,Tline.__len__()):
		Oline.append(round((CPU[i]*Tline[i])-Tline[0],4))
	O.append(Oline)

# Calcolo efficienza
E=[]
for Sline in S:
	Eline=[]
	for i in range(0,Sline.__len__()):
		Eline.append(round(Sline[i]/CPU[i],4))
	E.append(Eline)

# Plotting Tempo
plt.figure()
plt.grid()
plt.suptitle('Time Resource', fontsize=14, fontweight='bold')
plt.xlabel('CPU')
plt.ylabel('Time (us)')
for i in range(0,T.__len__()):
	plt.plot(CPU, T[i], '%s--o'%C[i], label='%s'%DSIZE[i], mfc='%s'%CLR[i])
plt.xlim([0,CPUMAX])
plt.ylim([0,max(T)])
plt.legend(loc=1)
plt.savefig("plottime.png")
# Make LaTex table file
writeTexTable("time.tex",T)


# Plotting Speed-Up
plt.figure()
plt.grid()
plt.suptitle('Speed-Up', fontsize=14, fontweight='bold')
plt.xlabel('CPU')
plt.ylabel('Speed-Up (T(1)/T(n))')
for i in range(0,S.__len__()):
	plt.plot(CPU, S[i], '%s--o'%C[i], label='%s'%DSIZE[i], mfc='%s'%CLR[i])
plt.xlim([0,CPUMAX])
plt.ylim([0,max(S)])
plt.legend(loc=1)
plt.savefig("plotspeedup.png")
# Make LaTex table file
writeTexTable("speedup.tex",S)


# Plotting Overhead
plt.figure()
plt.grid()
plt.suptitle('Overhead', fontsize=14, fontweight='bold')
plt.xlabel('CPU')
plt.ylabel('Overhead (nT(n)-T(1))')
for i in range(0,O.__len__()):
	plt.plot(CPU, O[i], '%s--o'%C[i], label='%s'%DSIZE[i], mfc='%s'%CLR[i])
plt.xlim([0,CPUMAX])
plt.ylim([0,max(O)])
plt.legend(loc=1)
plt.savefig("plotoverhead.png")
# Make LaTex table file
writeTexTable("overhead.tex",O)


# Plotting Efficienza
plt.figure()
plt.grid()
plt.suptitle('Efficiency', fontsize=14, fontweight='bold')
plt.xlabel('CPU')
plt.ylabel('Efficiency (S(n)/n)')
for i in range(0, E.__len__()):
	plt.plot(CPU, E[i], '%s--o'%C[i], label='%s'%DSIZE[i], mfc='%s'%CLR[i])
plt.xlim([0,CPUMAX])
plt.ylim([0,max(E)])
plt.legend(loc=1)
plt.savefig("plotefficiency.png")
# Make LaTex table file
writeTexTable("efficiency.tex",E)

_EOF_

cd data
python plotdata.py
rm plotdata.py
cd ..

