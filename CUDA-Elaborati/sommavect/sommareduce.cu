/*******************************************
* Code for CUDA System only
* Author: ascarius
* Date: Thu Nov 18 17:35:44 CEST 2015
********************************************/

#include <cuda.h>
#include <stdio.h>
#include <stdlib.h>

// N. max di thread per blocco supportato
#define MAX_BLOCK_SIZE 1024

// N. di thread per blocco
#ifndef BLOCK_SIZE
	#define BLOCK_SIZE MAX_BLOCK_SIZE
#endif

// N. di elementi da sommare
#ifndef DIM
	#define DIM	1024
#endif

__global__  void vectorsum_kernel(float * input, float * output, int len) {
	// Caricamento di un segmento del vettore di input nella memoria condivisa del blocco
	__shared__ float partialSum[2*BLOCK_SIZE];
	
	// Identificazione dell'id globale del Thread attivo nella griglia
	int globTID = (blockIdx.x * blockDim.x) + threadIdx.x;
	unsigned int TID = threadIdx.x; //ThreadID
	unsigned int start = 2 * blockIdx.x * blockDim.x;

	partialSum[TID] = 0.0;
	if ((start + TID) < len) partialSum[TID] = input[start + TID];	  

	partialSum[blockDim.x + TID] = 0.0;
	if ((start + blockDim.x + TID) < len) partialSum[blockDim.x + TID] = input[start + blockDim.x + TID];

	// Applicazione dell'algoritmo di "traverse reduction tree"
	for (unsigned int stride = blockDim.x; stride > 0; stride /= 2){
	  __syncthreads();
		if (TID < stride) partialSum[TID] += partialSum[TID + stride];
	}
	__syncthreads();

	// Scrittura della somma parziale ricavata dal blocco nella giusta posizione del vettore di output
	if (TID == 0 && (globTID*2) < len) {
		output[blockIdx.x] = partialSum[TID];
	}
}

int main(int argc, char ** argv) {
	int ii;

	float * hostInput; // Vettore di input 1D
	float * hostOutput; // Vettore di output 1D
	float * deviceInput; // Copia GPU del vettore di input
	float * deviceOutput; // Copia GPU del vettore di output

	int inputDIM = DIM; // Dimensione passata da riga di comando
	if ((argc>1) && (sscanf(argv[1], "%i", &inputDIM)!=1)) { 
		printf ("ERROR: passed parameter are not an integer\n");
		printf ("usage: %s [DIM]\n", argv[0]); 
		return -1;
	}

	int numInputElements = inputDIM; // Numero di elementi nel vettore di input
	int numOutputElements; // Numero di elementi nel vettore di output
   

	#ifdef TIMETEST
	float milliseconds = 0;
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	#endif


	srand(time(NULL));
	

	// Alloco il vettore di input sulla memoria lato host	
	hostInput = (float *) malloc(sizeof(float) * numInputElements);

	// Inizializzo il vettore di input con numeri casuali sulla CPU
	for (int i=0; i < inputDIM; i++) hostInput[i] = (rand()%10);


	#ifdef DEBUG
	printf("Il vettore di input è il seguente:\n");
	for (int i=0; i < inputDIM; i++) printf("%.1f ", hostInput[i]);
	printf("\n");
	#endif


	// Calcolo le dimensioni del vettore di output per l'applicazione dell'algoritmo
	numOutputElements = numInputElements / (BLOCK_SIZE<<1);
	if (numInputElements % (BLOCK_SIZE<<1)) numOutputElements++;

	// Alloco il vettore di output sulla memoria lato host	
	hostOutput = (float*) malloc(numOutputElements * sizeof(float));

	// Alloco la memoria GPU	 
	cudaMalloc((void **)&deviceInput, numInputElements * sizeof(float));
	cudaMalloc((void **)&deviceOutput, numOutputElements * sizeof(float));

	// Copio i dati dalla memoria host CPU alla memoria device GPU 
	cudaMemcpy(deviceInput, hostInput, numInputElements * sizeof(float), cudaMemcpyHostToDevice);

	// Inizializzo le dimensioni di griglia e blocchi in 1D
	dim3 DimGrid(numOutputElements, 1, 1);
	dim3 DimBlock(BLOCK_SIZE, 1, 1);


	#ifdef TIMETEST
	cudaEventRecord(start);
	#endif

	// Avvio il codice per il kernel device
	vectorsum_kernel<<<DimGrid, DimBlock>>>(deviceInput, deviceOutput, numInputElements);


	#ifdef TIMETEST
	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	#endif


	// Copio i dati risultato di nuovo sulla memoria della CPU host
	cudaMemcpy(hostOutput, deviceOutput, numOutputElements * sizeof(float), cudaMemcpyDeviceToHost);

	// Effettuo la riduzione del vettore di output sulla CPU host
	for (ii = 1; ii < numOutputElements; ii++) {
		hostOutput[0] += hostOutput[ii];
	}


	#ifndef TIMETEST
	printf("La somma dei %d numeri calcolati sulla GPU è: %.1f\n", inputDIM, hostOutput[0]);   
	#else
	cudaEventElapsedTime(&milliseconds, start, stop);
	cudaEventDestroy(start);
	cudaEventDestroy(stop);
	printf("L'algoritmo ha sommato %d numeri su una GPU CUDA (facendo uso di %d blocchi e %d thread per blocco) in %.0f us, con un bandwidth effettivo di %.2f MB/s\n", inputDIM, DimGrid.x, DimBlock.x, (milliseconds*1e3), ((numInputElements+numOutputElements)/(milliseconds*1e3)));   
	#endif


	// Libero la memoria GPU e CPU usata
	cudaFree(deviceInput);
	cudaFree(deviceOutput); 
	free(hostInput);
	free(hostOutput);

	return 0;
}
