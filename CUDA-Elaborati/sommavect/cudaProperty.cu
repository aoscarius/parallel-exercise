/*******************************************
* Code for CUDA System only
* Author: ascarius
* Date: Thu Nov 18 17:35:44 CEST 2015
********************************************/

#include <cuda.h> 
#include <stdlib.h> 
#include <stdio.h> 

__global__ void kernel( void){ 
} 

int ConvertSMVer2Cores(const int major, const int minor){
	if (major==1 && minor==3) {
    	return 8;
	} else if (major==2 && minor==0) {    
		return 32;
	} else if (major==2 && minor==1) {
    	return 48;
	} else {
		return -1;
	}
}

int main ( void){ 	
	int i, count;  
	cudaDeviceProp deviceProp;  
	cudaGetDeviceCount(&count); 
	
	printf("Found %d CUDA Capability Device\n\n", count);
	for (i=0; i<count; i++){
		cudaGetDeviceProperties(&deviceProp, i);

		printf("CUDA Device[%d]: %s\n", i, deviceProp.name);
		printf("General information:\n");
		printf("\tCompute capability: %d.%d\n", deviceProp.major, deviceProp.minor);
		printf("\tClock rate: %d Hz\n", deviceProp.clockRate);
		printf("\tDevice copy overlap: " );  
		if (deviceProp.deviceOverlap)	printf( "Enabled\n" );
		else printf( "Disabled\n" );
		printf("\tKernel execition timeout: ");
		if (deviceProp.kernelExecTimeoutEnabled) printf( "Enabled\n" );
		else printf( "Disabled\n" );

		printf("Memory information:\n");
		printf("\tTotal global mem: %ld bytes\n", deviceProp.totalGlobalMem);
		printf("\tTotal constant Mem: %ld bytes\n", deviceProp.totalConstMem);
		printf("\tMax mem pitch: %ld\n", deviceProp.memPitch );
		printf("\tTexture Alignment: %ld\n", deviceProp.textureAlignment);

		printf("MultiProcessor information:\n");
		printf("\tMultiprocessor count: %d\n", deviceProp.multiProcessorCount);
  		printf("\tCUDA Cores/MP: %d\n",  ConvertSMVer2Cores(deviceProp.major, deviceProp.minor));
		printf("\tCUDA Cores:  %d\n", ConvertSMVer2Cores(deviceProp.major, deviceProp.minor) * deviceProp.multiProcessorCount);
		printf("\tShared mem per mp: %ld bytes\n", deviceProp.sharedMemPerBlock);
		printf("\tRegisters per mp: %d\n", deviceProp.regsPerBlock);
		printf("\tThreads in warp: %d\n", deviceProp.warpSize);
		printf("\tMax threads per block: %d\n", deviceProp.maxThreadsPerBlock);
		printf("\tMax thread dimensions: (%d, %d, %d)\n", deviceProp.maxThreadsDim[0], deviceProp.maxThreadsDim[1], deviceProp.maxThreadsDim[2]);
		printf("\tMax grid dimensions: (%d, %d, %d)\n", deviceProp.maxGridSize[0], deviceProp.maxGridSize[1], deviceProp.maxGridSize[2]);
		printf("\n" );
	}
	return 0;  
}
