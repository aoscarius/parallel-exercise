/*******************************************
* Code for CPU System only
* Author: ascarius
* Date: Thu Nov 18 17:35:44 CEST 2015
********************************************/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#ifndef DIM
	#define DIM 1000
#endif

int main(int argc, char ** argv) {
	int i;
	float milliseconds, sum, * vectInput;

	#ifdef TIMETEST
  	clock_t t;
	#endif

	int inputDIM = DIM; // Dimensione passata da riga di comando
	if ((argc>1) && (sscanf(argv[1], "%i", &inputDIM)!=1)) { 
		printf ("ERROR: passed parameter are not an integer\n");
		printf ("usage: %s [DIM]\n", argv[0]); 
		return -1;
	}


	srand(time(NULL));

	// Inizializzo il vettore di input con numeri casuali sulla CPU
	vectInput = (float *) malloc(sizeof(float) * inputDIM);
	for (i=0; i < inputDIM; i++) vectInput[i] = (rand()%10);

	#ifdef DEBUG
	printf("Il vettore di input è il seguente:\n");
	for (i=0; i < inputDIM; i++) printf("%.1f ", vectInput[i]);
	printf("\n");
	#endif

	#ifdef TIMETEST
  	t = clock();
	#endif

	sum=0;
	for (i=0; i<inputDIM; i++) sum = sum + vectInput[i];	

	#ifndef TIMETEST
	printf("La somma dei %d numeri calcolati sulla CPU è: %.1f\n", inputDIM, sum);   
	#else
  	t = clock() - t;
	milliseconds = (((float)t)/CLOCKS_PER_SEC)*1e6;
 	printf("L'algoritmo ha sommato %d numeri su una CPU (facendo uso di %d blocchi e %d thread per blocco) in %.0f us, con un bandwidth effettivo di %.2f MB/s\n", inputDIM, 0, 0, milliseconds, ((2*inputDIM)/(milliseconds*1e3)));   
	#endif

	// Libero la memoria CPU usata
	free(vectInput);

	return 0;
}
