#!/bin/bash

#/*******************************************
#* Author: Oscar Castello
#********************************************/

##########################
#                        #
#   The PBS directives   #
#                        #
##########################
#PBS -q studenti
#PBS -l nodes=1:ppn=8
#PBS -N integral
#PBS -o t_integral.out
#PBS -e t_integral.err

##########################################
#                                        #
#   Auxiliary bash script variable       #
#                                        #
##########################################
CURFOLDER=integral
CURFNAME=integral

N=(500000 1000000 5000000 10000000 50000000 100000000)
THREAD=(1 2 4 6 8)

##########################################
#                                        #
#   Output some useful job information.  #
#                                        #
##########################################

NCPU=`wc -l < $PBS_NODEFILE`
echo ------------------------------------------------------
echo 'This job is allocated on '${NCPU}' cpu(s)'
echo 'Job is running on node(s): '
cat $PBS_NODEFILE
echo

PBS_O_WORKDIR=$PBS_O_HOME/$CURFOLDER
echo ------------------------------------------------------
echo PBS: qsub is running on $PBS_O_HOST
echo PBS: originating queue is $PBS_O_QUEUE
echo PBS: executing queue is $PBS_QUEUE
echo PBS: working directory is $PBS_O_WORKDIR
echo PBS: execution mode is $PBS_ENVIRONMENT
echo PBS: job identifier is $PBS_JOBID
echo PBS: job name is $PBS_JOBNAME
echo PBS: node file is $PBS_NODEFILE
echo PBS: current home directory is $PBS_O_HOME
echo PBS: PATH = $PBS_O_PATH
echo ------------------------------------------------------
echo

export PSC_OMP_AFFINITY=TRUE

echo "Eseguo il test di efficienza sull'algoritmo: $CURFNAME.c" 

STARTTIME=$(date +%s)
for Ni in ${N[@]}
do
	/usr/lib64/openmpi/1.4-gcc/bin/mpicc -fopenmp -lgomp -o $PBS_O_WORKDIR/$CURFNAME -D DIVS=$Ni -D TIMETEST $PBS_O_WORKDIR/$CURFNAME.c -lm
	for THREADi in ${THREAD[@]}
	do
		export OMP_NUM_THREADS=$THREADi
		/usr/lib64/openmpi/1.4-gcc/bin/mpiexec -machinefile $PBS_NODEFILE -np 1 $PBS_O_WORKDIR/$CURFNAME
	done
done
ENDTIME=$(date +%s)

echo "Testing eseguito in $(expr $ENDTIME - $STARTTIME) secondi."
echo
